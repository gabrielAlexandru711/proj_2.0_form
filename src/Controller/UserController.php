<?php

namespace App\Controller;

use App\Entity\Blacklist;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\User;
use Symfony\Component\HttpFoundation\Request;
use App\Form\UserFormType;

class UserController extends AbstractController
{
    /**
     * @Route("/user", name="user")
     */
    public function index(Request $request)
    {
        $ip = $request->getClientIp();
        $checkIP = $this->getDoctrine()->getRepository(Blacklist::class)->findOneBy(['ip' => $ip]);

        if($checkIP) {
            return $this->render('user/banned.html.twig');
        }

        $user = new User();

        $form = $this->createForm(UserFormType::class, $user);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $user = $form->getData();

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($user);
            $entityManager->flush();


        }

        return $this->render('user/index.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}
