<?php

namespace App\Validator;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Doctrine\ORM\EntityManagerInterface;

class EmailValidator extends ConstraintValidator
{
    private $manager;

    public function __construct(EntityManagerInterface $manager)
    {
        $this->manager = $manager;
    }

    public function validate($email, Constraint $constraint)
    {
        if (null === $email || '' === $email) {
            return;
        }

        $entityManager = $this->manager;
        $qb = $entityManager->createQueryBuilder()
            ->select('count(u.email)')
            ->from('App:User', 'u')
            ->where('u.email = ?1')
            ->setParameter(1, $email);

        $count = $qb->getQuery()->getSingleScalarResult();

        if($count == $constraint->x) {
            $this->context->buildViolation($constraint->message)
                ->setParameter('{{ value }}', $email)
                ->addViolation();
        }
    }
}
