<?php

namespace App\Entity;

use App\Repository\UserRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use App\Validator as AcmeAssert;

/**
 * @ORM\Entity(repositoryClass=UserRepository::class)
 */
class User
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=1)
     * @Assert\NotBlank(message="Te rugam alege-ti sexul")
     * @Assert\Regex(pattern="/F|M/",message="Sexul poate sa fie doar (M)Male sau (F)Female")
     * @Assert\Length(max=1, maxMessage="Sexul nu poate fi mai lung de 1 caracter")
     */
    private $sex;

    /**
     * @ORM\Column(type="string", length=19)
     * @Assert\NotBlank(message="Introduceti-va numele")
     * @Assert\Length(max=19, maxMessage="Numele nu poate fi mai lung de 19 caractere")
     * @Assert\Type(type="alnum", message="Caractere interzise")
     */
    private $nume;

    /**
     * @ORM\Column(type="string", length=14)
     * @Assert\NotBlank(message="Introduceti-va prenumele")
     * @Assert\Length(max=14, maxMessage="Prenumele nu poate fi mai lung de 14 caractere")
     * @Assert\Type(type="alnum", message="Caractere interzise")
     */
    private $prenume;

    /**
     * @ORM\Column(type="string", length=38, nullable=true)
     * @Assert\Regex(pattern="/^[A-Za-z0-9_ ',\-.\;]+$/",message="Doar caractere alfanumerice, spatiu si urmatoarele caractere -,.;’ sunt permise")
     * @Assert\Length(max=38, maxMessage="Adresa nu poate fi mai lunga de 38 caractere")
     */
    private $adresa;

    /**
     * @ORM\Column(type="string", length=32)
     * @Assert\NotBlank(message="Introduceti Orasul")
     * @Assert\Regex(pattern="/^[A-Za-z0-9_ '\-]+$/",message="Doar caractere alfanumerice, spatiu si urmatoarele caractere -’ sunt permise")
     * @Assert\Length(max=32, maxMessage="Orasul nu poate fi mai lung de 32 caractere")
     */
    private $oras;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(message="Introduceti Email-ul")
     * @Assert\Email(message="Email-ul nu este valid")
     * @AcmeAssert\Email(x=2)
     */
    private $email;

    /**
     * @ORM\Column(type="boolean")
     * @Assert\Regex(pattern="/1/",message="Accordul trebuie sa fie true(1)")
     */
    private $accord;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSex(): ?string
    {
        return $this->sex;
    }

    public function setSex(string $sex): self
    {
        $this->sex = $sex;

        return $this;
    }

    public function getNume(): ?string
    {
        return $this->nume;
    }

    public function setNume(string $nume): self
    {
        $this->nume = $nume;

        return $this;
    }

    public function getPrenume(): ?string
    {
        return $this->prenume;
    }

    public function setPrenume(string $prenume): self
    {
        $this->prenume = $prenume;

        return $this;
    }

    public function getAdresa(): ?string
    {
        return $this->adresa;
    }

    public function setAdresa(?string $adresa): self
    {
        $this->adresa = $adresa;

        return $this;
    }

    public function getOras(): ?string
    {
        return $this->oras;
    }

    public function setOras(string $oras): self
    {
        $this->oras = $oras;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getAccord(): ?bool
    {
        return $this->accord;
    }

    public function setAccord(bool $accord): self
    {
        $this->accord = $accord;

        return $this;
    }
}
